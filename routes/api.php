<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('teste', 'ApiController@index');
Route::get('getbalance', 'ApiController@getBalance');
Route::get('gettransaction', 'ApiController@getTransaction');
Route::get('getnewaddress', 'ApiController@getNewAddress');
Route::get('getaccountaddress', 'ApiController@getAccountAddress');
Route::get('getaccount', 'ApiController@getAccount');
Route::get('sendtoaddress', 'ApiController@sendToAddress');
Route::get('sendfrom', 'ApiController@sendFrom');
Route::get('move', 'ApiController@move');
Route::get('moveUser', 'ApiController@moveUser');
Route::get('getwallet', 'ApiController@getWallet');
Route::get('listtransactions', 'ApiController@listTransactions');
Route::get('getbalanceapi', 'ApiController@getBalanceAPI');
Route::get('listaccounts', 'ApiController@listaccounts');