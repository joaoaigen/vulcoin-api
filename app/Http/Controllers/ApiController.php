<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use liamwli\PHPBitcoin\BitCoin;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $de = '';
        $para = '';
        $quantidade = '';
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');

        $info = $con->getbalance();

        return response()->json([
            'status' => $con->getStatus(),
            'quantidade' => $info,
        ]);
        /*
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->gettransaction('9d64d99618fb903c4ae3617095740d287b556b5fb7f6851f8bb213d382c7fd55');
        //Vs1BW7pHckGCkBBxVqae4efEfT5EnZisVK
        //9d64d99618fb903c4ae3610095740d287b556b5fb7f6851f8bb213d382c7fd55
        //VqgauHqqXzoyE6nRdFwZ6TmBfYcn7AtKQq
        return response()->json([
            'status' => $con->getStatus(),
            'quantidade' => $info,
            'conta' => 'testeapi',
            'erro' => $con->getError()
        ]);  */
    }

    public function getNewAddress(Request $request){

        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getnewaddress($request->account);
        $pub_key = $con->getnewpubkey($request->account);

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'carteira' => $info,
                'conta' => $request->account,
                'pub_key' => $pub_key
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $info,
                'conta' => $request->account
            ]);
    }

    public function getAccountAddress(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getaccountaddress($request->account);

        return response()->json([
            'status' => $con->getStatus(),
            'carteira' => $info,
            'conta' => $request->account
        ]);
    }

    public function getAccount(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getaccount($request->vulcoinaddress);

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'carteira' => $request->vulcoinaddress,
                'conta' => $info
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $info,
                'conta' => $request->account
            ]);
    }

    public function getBalance(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getbalance($request->account);

        return response()->json([
            'status' => $con->getStatus(),
            'quantidade' => $info,
            'conta' => $request->account
        ]);
    }

    public function getNewPubKey(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getnewpubkey($request->account);

        return response()->json([
            'status' => $con->getStatus(),
            'pubkey' => $info,
            'conta' => $request->account
        ]);
    }


    public function sendFrom(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->sendfrom($request->account, $request->address, floatval($request->amount));
        $saldo = $con->getbalance($request->account);

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'hash' => $info,
                'de' => $request->account,
                'para' => $request->address,
                'saldo_atual' => $saldo
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $con->getError(),
            ]);
    }

    public function sendToAddress(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');

        $info = $con->sendtoaddress($request->address, floatval($request->amount));

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'hash' => $info,
                'carteira' => $request->address
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $con->getError(),
            ]);
    }

    public function validatePubKey(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->validatepubkey($request->key);

        return response()->json([
            'status' => $con->getStatus(),
            'info' => $info,
        ]);
    }

    public function getTransaction(Request $request){
        $de = '';
        $para = '';
        $quantidade = '';
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->gettransaction($request->hash);

        if(isset($info['details'][1])){
            $de = $info['vout'][0]['scriptPubKey']['addresses'][0];
            $para = $info['vout'][1]['scriptPubKey']['addresses'][0];
            $quantidade = $info['amount'];
            $confirmacoes = $info['confirmations'];
            $hora_recebimento = $info['timereceived'];
        }else{
            $de = $info['vout'][0]['scriptPubKey']['addresses'][0];
            $confirmacoes = $info['confirmations'];
            $hora_recebimento = $info['timereceived'];
            $para = $info['details'][0]['address'];
            $quantidade = $info['details'][0]['amount'];
        }

        return response()->json([
            'status' => $con->getStatus(),
            'de' => $de,
            'para' => $para,
            'quantidade' => $quantidade,
            'confirmacoes' => $confirmacoes,
            'hora_recebimento' => $hora_recebimento,
            'erro' => $con->getError()
        ]);
    }

    public function move(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');

        $info = $con->move($request->account, '', floatval($request->amount));

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'info' => $info,
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $con->getError(),
            ]);
    }

    public function getWallet(){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getaccountaddress('');

        return response()->json([
            'status' => $con->getStatus(),
            'carteira' => $info,
        ]);
    }

    public function moveUser(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');

        $info = $con->move(isset($request->account) ? $request->account : '', $request->toaccount, floatval($request->amount));

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'info' => $info,
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $con->getError(),
            ]);
    }

    public function listTransactions(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');

        $info = $con->listtransactions($request->account);

        if($con->getStatus() == 200)
            return response()->json([
                'status' => $con->getStatus(),
                'info' => $info,
            ]);
        else
            return response()->json([
                'status' => $con->getStatus(),
                'erro' => $con->getError(),
            ]);
    }

    public function getBalanceAPI(Request $request){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->getbalance('');

        return response()->json([
            'status' => $con->getStatus(),
            'quantidade' => $info,
        ]);
    }


    public function listaccounts(){
        $con = new BitCoin('user', '0Dz34IpAD3kolr9', '127.0.0.1', '33656');
        $info = $con->listaccounts();

        return response()->json([
            'status' => $con->getStatus(),
            'contas' => $info,
        ]);
    }

}
